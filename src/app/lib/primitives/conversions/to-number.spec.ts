import { toNumber } from './to-number'

const whitespace = '    \t\r\n  '

function check(
  description: string,
  value: any,
  expectation: any,
): void {
  it(description, () =>
    expect(
      toNumber(value),
    ).toEqual(expectation),
  )
}

describe('toNumber', () => {
  // General types
  check('handles null', null, null)
  check('handles undefined', undefined, null)

  check('handles string empty', '', null)
  check('handles string non-empty', 'something', null)
  check('handles white space', whitespace, null)

  check('handles zero', 0, 0)
  check('handles number', 7.2, 7.2)
  check('handles 0o144 as a number', 0o144, 100)
  check('handles 0xff as number', 0xff, 255)
  check('handles NaN', Number.NaN, null)
  check('handles Infinity', Number.POSITIVE_INFINITY, null)

  check('handles bigint', BigInt(7), 7)

  check('handles object', {}, null)

  check('handles boolean true', true, null)
  check('handles boolean false', false, null)

  check('handles Symbol', Symbol(), null)

  check('handles Date', new Date(), null)

  check('handles function', () => 1, null)

  // Other number forms
  check('handles -10 as a string', '-10', -10)
  check('handles +10 as a string', '+10', 10)
  check('handles 0o144 as a string', '0o144', 100)
  check('handles 0 as a string', '0', 0)
  check('handles 0xff as a string', '0xFF', 255)
  check('handles 8e5 as a string', '8e5', 800_000)
  check('handles 3.1415 as a string', '3.1415', 3.1415)
  check('handles -0x42', '-0x42', null)
  check('handles 7.2asdf', '7.2asdf', null)
  check('handles number with whitespace', '   \t\r\n -10    \t \r \n  ', -10)
})

