import { isValidDate } from '../is/is-valid-date'

/**
 * Convert to a `Date` object.
 *
 * If conversion fails, fall back to a default value.
 * The default value can be anything.
 *
 * For example `toDateWithDefault(x, null)`
 * will return `null` to indicate the conversion failed.
 *
 * NOTE:
 *
 * JavaScript handling of dates and times is really poor.
 * This has been a long-standing known problem.
 *
 * For example, Chrome as of 2022-11-10
 * doesn't handle ISO-8601 basic format at all.
 *
 * APIs are assumed to transfer date/times in ISO-8601 UTC.
 */
export function toDateWithDefault<T>(
  value: any,
  defaultValue: T,
): Date | T {
  switch (typeof value) {
    case 'number':
      value = new Date(value)
      break
    case 'string':
      value = new Date(value)
      break
    case 'bigint':
      value = new Date(Number(value))
      break
  }

  return isValidDate(value) ? value : defaultValue
}
