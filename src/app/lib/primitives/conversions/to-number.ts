import { toNumberWithDefault } from './to-number-with-default'

export function toNumber(value: any): number | null {
  return toNumberWithDefault(value, null)
}
