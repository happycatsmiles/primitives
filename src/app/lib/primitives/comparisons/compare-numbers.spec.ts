import { compareNumbers } from './compare-numbers'

/** "Success" means expected value is returned. */
function check(
  description: string,
  a: number,
  b: number,
  expectation: -1 | 0 | 1,
): void {
  it(description, () => {
    expect(
      compareNumbers(a, b),
    ).toBe(expectation)
  })
}

describe('compareNumbers', () => {
  check('handles 0,0', 0, 0, 0)
  check('handles 0,1', 0, 1, -1)
  check('handles 1,0', 1, 0, 1)
  check('handles 1,1', 1, 1, 0)
})

