/**
 * For sorting values.
 *
 * @returns a value indicating the relative order of the parameters:
 * -1: a < b
 *  0: a = b
 *  1: a > b
 */
export function compareValues<T>(a: T, b: T): -1 | 0 | 1 {
  return a > b
    ? 1
    : (a < b ? -1 : 0)
}
