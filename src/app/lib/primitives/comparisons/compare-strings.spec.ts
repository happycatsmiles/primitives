import { compareStrings } from './compare-strings'

/** "Success" means expected value is returned. */
function check(
  description: string,
  a: string,
  b: string,
  expectation: -1 | 0 | 1,
): void {
  it(description, () => {
    expect(
      compareStrings(a, b),
    ).toBe(expectation)
  })
}

describe('compareStrings', () => {
  check('handles "a","a"', 'a', 'a', 0)
  check('handles "a","b"', 'a', 'b', -1)
  check('handles "b","a"', 'b', 'a', 1)
  check('handles "b","b"', 'b', 'b', 0)
})

