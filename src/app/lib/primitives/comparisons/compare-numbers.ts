import { compareValues } from './compare-values'

/**
 * For sorting numbers.
 */
export const compareNumbers =
  (a: number, b: number): -1 | 0 | 1 => compareValues<number>(a, b)
