import { SetOperators } from './set-operators'

const emptySet = new Set()
const set1 = new Set([1])
const set2 = new Set([2])
const set12 = new Set([1, 2])

const setOperator = <T>(a: Set<T>, b: Set<T>): Set<T> =>
  SetOperators.difference(a, b)

describe('SetOperators.difference', () => {
  it('{} - {} = {}', () => expect(
    setOperator(emptySet, emptySet),
  ).toEqual(emptySet))

  it('{1} - {} = {1}', () => expect(
    setOperator(set1, emptySet),
  ).toEqual(set1))

  it('{} - {1} = {}', () => expect(
    setOperator(emptySet, set1),
  ).toEqual(emptySet))

  it('{1} - {1} = {}', () => expect(
    setOperator(set1, set1),
  ).toEqual(emptySet))

  it('{1} - {2} = {1}', () => expect(
    setOperator(set1, set2),
  ).toEqual(set1))

  it('{2} - {1} = {2}', () => expect(
    setOperator(set2, set1),
  ).toEqual(set2))

  it('{} - {1,2} = {}', () => expect(
    setOperator(emptySet, set12),
  ).toEqual(emptySet))

  it('{1,2} - {} = {1,2}', () => expect(
    setOperator(set12, emptySet),
  ).toEqual(set12))

  it('{1} - {1,2} = {}', () => expect(
    setOperator(set1, set12),
  ).toEqual(emptySet))

  it('{1,2} - {1} = {2}', () => expect(
    setOperator(set12, set1),
  ).toEqual(set2))

  it('{1,2} - {1,2} = {}', () => expect(
    setOperator(set12, set12),
  ).toEqual(emptySet))
})
