import { SetOperators } from './set-operators'

const emptySet = new Set()
const set1 = new Set([1])
const set12 = new Set([1, 2])

const setOperator = <T>(a: Set<T>): boolean =>
  SetOperators.empty(a)

describe('SetOperators.empty', () => {
  it('{} = {}', () => expect(
    setOperator(emptySet),
  ).toEqual(true))

  it('{1} != {}', () => expect(
    setOperator(set1),
  ).toEqual(false))

  it('{1,2} != {}', () => expect(
    setOperator(set12),
  ).toEqual(false))
})
