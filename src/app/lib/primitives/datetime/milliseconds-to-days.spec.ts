import { millisecondsToDays } from './milliseconds-to-days'

describe('millisecondsToDays', () => {
  it('handles -2 days correctly', () => {
    expect(millisecondsToDays(-2 * 24 * 3600 * 1000)).toBe(-2)
  })

  it('handles -1 days correctly', () => {
    expect(millisecondsToDays(-1 * 24 * 3600 * 1000)).toBe(-1)
  })

  it('handles -0.5 days correctly', () => {
    expect(millisecondsToDays(-0.5 * 24 * 3600 * 1000)).toBe(-0.5)
  })

  it('handles 0 days correctly', () => {
    expect(millisecondsToDays(0)).toBe(0)
  })

  it('handles 0.5 days correctly', () => {
    expect(millisecondsToDays(0.5 * 24 * 3600 * 1000)).toBe(0.5)
  })

  it('handles 1 days correctly', () => {
    expect(millisecondsToDays(24 * 3600 * 1000)).toBe(1)
  })

  it('handles 2 days correctly', () => {
    expect(millisecondsToDays(2 * 24 * 3600 * 1000)).toBe(2)
  })

})

