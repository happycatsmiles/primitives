import { MillisecondsPerDay } from './milliseconds-per-day'

/** Convert a of milliseconds to day. */
export function millisecondsToDays(milliseconds: number): number {
  return milliseconds / MillisecondsPerDay
}
