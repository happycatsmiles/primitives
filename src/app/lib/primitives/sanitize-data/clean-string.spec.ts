import { Clean } from './clean'

const whitespace = '    \t\r\n  '

/** "Success" means expected value is returned. */
function check(
  description: string,
  value: any,
  expectation: any,
): void {
  it(description, () =>
    expect(
      Clean.string(value),
    ).toEqual(expectation),
  )
}

describe('Clean.string', () => {
  // General types
  check('handles null', null, undefined)
  check('handles undefined', undefined, undefined)

  check('handles string empty', '', '')
  check('handles string non-empty', 'SoMeThInG', 'SoMeThInG')
  check('handles white space', whitespace, whitespace)

  check('handles zero', 0, undefined)
  check('handles number', 7.2, undefined)
  check('handles 0o144 as a number', 0o144, undefined)
  check('handles 0xff as number', 0xff, undefined)
  check('handles NaN', Number.NaN, undefined)
  check('handles Infinity', Number.POSITIVE_INFINITY, undefined)

  check('handles bigint', BigInt(7), undefined)

  check('handles object', {}, undefined)

  check('handles boolean true', true, undefined)
  check('handles boolean false', false, undefined)

  check('handles Symbol', Symbol(), undefined)

  const d = new Date()
  check('handles Date', d, undefined)

  check('handles function', () => 1, undefined)
})

