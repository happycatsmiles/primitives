import { toNumberWithDefault } from '../conversions/to-number-with-default'

export const NULL_UUID = '00000000-0000-0000-0000-000000000000'
const UUID_REGEX = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/

/**
 * This "cleans" untrusted data coming in from an external source.
 *
 * Each method will return `undefined` if the validation fails.
 */
export class Clean {

  /**
   * Extract and validate an array of elements of the same type.
   *
   * @param value - data to clean
   * @param cleaner - validation function for individual elements
   */
  static array<TElement>(
    value: any,
    cleaner: (value: any) => TElement | undefined,
  ): TElement[] | undefined {
    if (!Array.isArray(value)) return undefined

    const cleaned = value.map(x => cleaner(x))
    return cleaned.some(x => x === undefined)
      ? undefined
      : cleaned as TElement[]
  }

  /**
   * Extract and validate a boolean value.
   * If `value` is a string, it must be either `true` or `false`.
   * If `value` is a boolean, return it unchanged.
   *
   * @param value - value to clean
   */
  static boolean(value: any): boolean | undefined {
    if (typeof value === 'string')
      return value === 'true' ? true : (value === 'false' ? false : undefined)
    else
      return typeof value === 'boolean' ? value : undefined
  }

  /**
   * Extract and validate a numeric value.
   *
   * @param value - value to clean
   */
  static number(value: any): number | undefined {
    return toNumberWithDefault<undefined>(value, undefined)
  }

  /**
   * Extract and validate a string value.
   *
   * @param value - value to clean
   */
  static string(value: any): string | undefined {
    return typeof value === 'string' ? value : undefined
  }

  /**
   * Extract and validate a trimmed string value.
   *
   * @param value - value to clean
   */
  static stringTrim(value: any): string | undefined {
    return typeof value === 'string' ? value.trim() : undefined
  }

  /**
   * Extract and validate a trimmed, lower-case string value.
   */
  static stringTrimLower(value: any): string | undefined {
    return typeof value === 'string' ? value.trim().toLowerCase() : undefined
  }

  /**
   * Extract and validate a `Date` value.
   *
   * @param value - value to clean
   */
  static timestamp(value: any): Date | undefined {
    if (typeof value === 'string') {
      const result = Date.parse(value)
      return Number.isNaN(result) ? undefined : new Date(result)
    }
    if (typeof value === 'number') {
      const numericValue = Clean.number(value)
      return numericValue === undefined ? undefined : new Date(numericValue)
    }

    // Try to determine if this is a `Date` object.
    if (value === null) return undefined
    return (typeof value === 'object' && typeof value.getMilliseconds === 'function')
      ? value : undefined
  }

  /**
   * Extract and validate a UUID string.
   *
   * @param value - value to clean
   * @param allowNullUuid - allow `NULL_UUID` values.
   */
  static uuid(value: any, allowNullUuid = false): string | undefined {
    if (typeof value !== 'string') return undefined
    value = value.trim().toLowerCase()
    if (value === NULL_UUID && !allowNullUuid) return undefined
    return UUID_REGEX.test(value) ? value : undefined
  }

}
