import { Clean } from './clean'

const whitespace = '    \t\r\n  '

/** "Success" means expected value is returned. */
function check(
  description: string,
  value: any,
  expectation: any,
): void {
  it(description, () =>
    expect(
      Clean.boolean(value),
    ).toEqual(expectation),
  )
}

describe('Clean.boolean', () => {
  // General types
  check(`handles null`, null, undefined)
  check(`handles undefined`, undefined, undefined)

  check(`handles string empty`, ``, undefined)
  check(`handles string non-empty`, `SoMeThInG`, undefined)
  check(`handles white space`, whitespace, undefined)

  check(`handles value 'true'`, 'true', true)
  check(`handles value 't'`, whitespace, undefined)
  check(`handles value 'T'`, whitespace, undefined)
  check(`handles value 'TRUE'`, whitespace, undefined)

  check(`handles value 'false'`, 'false', false)
  check(`handles value 'f'`, whitespace, undefined)
  check(`handles value 'F'`, whitespace, undefined)
  check(`handles value 'FALSE'`, whitespace, undefined)

  check(`handles zero`, 0, undefined)
  check(`handles number`, 7.2, undefined)
  check(`handles 0o144 as a number`, 0o144, undefined)
  check(`handles 0xff as number`, 0xff, undefined)
  check(`handles NaN`, Number.NaN, undefined)
  check(`handles Infinity`, Number.POSITIVE_INFINITY, undefined)

  check(`handles bigint`, BigInt(7), undefined)

  check(`handles object`, {}, undefined)

  check(`handles boolean true`, true, true)
  check(`handles boolean false`, false, false)

  check(`handles Symbol`, Symbol(), undefined)

  const d = new Date()
  check(`handles Date`, d, undefined)

  check(`handles function`, () => 1, undefined)
})

