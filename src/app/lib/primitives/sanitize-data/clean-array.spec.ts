import { Clean } from './clean'

/** "Success" means expected value is returned. */
function check<T>(
  description: string,
  value: any,
  cleaner: (value: any) => T | undefined,
  expectation: any,
): void {
  it(description, () =>
    expect(
      Clean.array(value, cleaner),
    ).toEqual(expectation),
  )
}

describe('Clean.boolean', () => {
  check('Handles non-array', 'true', Clean.boolean, undefined)
  check('Handles empty array', [], Clean.boolean, [])
  check('Handles boolean array', [true, 'false'], Clean.boolean, [true, false])
  check('Handles bad boolean array', [true, 'false', 'T'], Clean.boolean, undefined)
})

