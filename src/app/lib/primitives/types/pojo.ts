/**
 * This is an interface that makes clear that the type is a
 * POJO; a "Plain Old Javascript Object".
 *
 * Thus, it has properties which can be accessed using the square
 * bracket syntax, i.e. `foo['bar']`.
 *
 * Some Javascript types, such as `undefined`, do not have this feature.
 * Technically `typeof null === 'object'`,
 * but one cannot read its properties.
 * `null` is an object, but not a POJO.
 */
export interface Pojo<T = any> {
  [key: string]: T;
}
