import {compareStrings} from '../comparisons/compare-strings';
import {Pojo} from '../types/pojo';
import {toString} from '../conversions/to-string';

/**
 * This is for the `sort` method to sort by a string field.
 * This sort is *not* case sensitive.
 *
 * Example:
 *
 * Given an array of objects with a field "name",
 * return an array sorted by this field:
 *
 * const sortedArray = myArray.sort(sortAlphabeticallyByProperty('name')
 */
export function sortAlphabeticallyByProperty<T extends Pojo>(property: string): (a: T, b: T) => number {
  return (a: T, b: T) =>
      compareStrings(
          toString(a[property]).toLowerCase(),
          toString(b[property]).toLowerCase(),
      );
}

