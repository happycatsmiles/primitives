/**
 * A simple Least Recently Used cache.
 *
 * This is a cache that will hold a certain number of elements.
 * Accessing an element in the cache pushes it to the front
 * of the list as it has now become the most recently used.
 *
 * Adding more elements than the maximum capacity will remove
 * the least recently used item.
 *
 *
 * Converted to Typescript from:
 *
 * https://stackoverflow.com/questions/996505/lru-cache-implementation-in-javascript
 *
 * > Since `Map` keeps insertion order,
 * >  adding a bit of code around it will get you a LRU and
 * > for most uses this should be plenty fast.
 * >
 * > I needed a simple LRU cache for
 * > a small number of expensive operations (1 second).
 * > I felt better about copy-pasting some small code
 * > rather than introducing something external,
 * > but since I didn't find it I wrote it:
 *
 */
export class LruCache<TKey, TValue> {

  private readonly cache = new Map<TKey, TValue>()

  constructor(private cacheSize = 1000) {
  }

  //////////////////////////////////////////////////////////////////////
  // Public Properties
  //////////////////////////////////////////////////////////////////////

  public forEach(
    callbackFn: (value: TValue, key: TKey, map: Map<TKey, TValue>) => void,
    thisArg?: any,
  ): void {
    this.cache.forEach(callbackFn, thisArg)
  }

  public get size(): number {
    return this.cache.size
  }

  //////////////////////////////////////////////////////////////////////
  // Public Methods
  //////////////////////////////////////////////////////////////////////

  public clear(): void {
    this.cache.clear()
  }

  public delete(key: TKey | null | undefined): void {
    if (key) {
      this.cache.delete(key)
    }
  }

  public get(key: TKey | null | undefined): TValue | undefined {
    if (key === null || key === undefined) {
      return undefined
    }

    const item = this.cache.get(key)

    if (item) {
      // refresh key
      this.cache.delete(key)
      this.cache.set(key, item)
    }

    return item
  }

  public set(key: TKey, value: TValue): void {
    // refresh key
    if (this.cache.has(key)) {
      this.cache.delete(key)
    } else if (this.cache.size >= this.cacheSize) {
      this.evictOldest()
    }

    this.cache.set(key, value)
  }

  public values(): IterableIterator<TValue> {
    return this.cache.values()
  }

  //////////////////////////////////////////////////////////////////////
  // Protected Methods
  //////////////////////////////////////////////////////////////////////

  /**
   * This is moved to a protected method so descendents
   * can know when an item gets evicted.
   * Descendents must call `super()`
   * to ensure the item actually gets evicted.
   *
   * @protected
   */
  protected evictOldest(): void {
    const oldest = this.cache.keys().next().value
    this.cache.delete(oldest)
  }

}
