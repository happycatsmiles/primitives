import { LruCache } from './lru-cache'

/**
 * Bypass TypeScript visibility to grab the cache values.
 */
function cacheValues<TKey, TValue>(cache: LruCache<TKey, TValue>): TValue[] {
  return [...(cache as any).cache.values()]
}

describe('LruCache', () => {

  it('should be empty to begin with', () => {
    const lru = new LruCache<string, number>(4)
    expect(cacheValues(lru)).toEqual([])
  })

  it('should accept up to max values.', () => {
    const lru = new LruCache<string, number>(4)
    lru.set('one', 1)
    lru.set('two', 2)
    lru.set('three', 3)
    lru.set('four', 4)
    expect(cacheValues(lru)).toEqual([1, 2, 3, 4])
  })

  it('should drop the least recent when adding more values.', () => {
    const lru = new LruCache<string, number>(4)
    lru.set('one', 1)
    lru.set('two', 2)
    lru.set('three', 3)
    lru.set('four', 4)
    lru.set('five', 5)
    expect(cacheValues(lru)).toEqual([2, 3, 4, 5])
  })

  it('should promote an item when accessing it.', () => {
    const lru = new LruCache<string, number>(4)
    lru.set('one', 1)
    lru.set('two', 2)
    lru.set('three', 3)
    lru.set('four', 4)
    lru.get('one')
    expect(cacheValues(lru)).toEqual([2, 3, 4, 1])
  })

  it('should return null when accessing an invalid key.', () => {
    const lru = new LruCache<string, number>(4)
    lru.set('one', 1)
    lru.set('two', 2)
    lru.set('three', 3)
    lru.set('four', 4)
    expect(lru.get('five')).toBe(undefined)
  })

  it('should promote an item when it is updated.', () => {
    const lru = new LruCache<string, number>(4)
    lru.set('one', 1)
    lru.set('two', 2)
    lru.set('three', 3)
    lru.set('four', 4)
    lru.set('one', -1)
    expect(cacheValues(lru)).toEqual([2, 3, 4, -1])
  })

  it('should be able to be cleared.', () => {
    const lru = new LruCache<string, number>(4)
    lru.set('one', 1)
    lru.set('two', 2)
    lru.set('three', 3)
    lru.set('four', 4)
    lru.clear()
    expect(cacheValues(lru)).toEqual([])
  })

  it('should report its size.', () => {
    const lru = new LruCache<string, number>(4)
    lru.set('one', -1)
    lru.set('two', -2)
    lru.set('three', -3)
    expect(lru.size).toEqual(3)
  })

  it('should allow items to be deleted.', () => {
    const lru = new LruCache<string, number>(4)
    lru.set('one', 1)
    lru.set('two', 2)
    lru.set('three', 3)
    lru.set('four', 4)
    lru.delete('three')
    expect(cacheValues(lru)).toEqual([1, 2, 4])
  })

  it('should support forEach.', () => {
    const lru = new LruCache<string, number>(4)
    lru.set('one', 1)
    lru.set('two', 2)
    lru.set('three', 3)
    lru.set('four', 4)

    const values: number[] = []
    const keys: string[] = []
    lru.forEach((value, key) => {
      values.push(value)
      keys.push(key)
    })
    expect(values).toEqual([1, 2, 3, 4])
    expect(keys).toEqual(['one', 'two', 'three', 'four'])
  })

})

