/**
 * Converts a string into null if it's empty or only contains whitespace.
 */
export function nullIfEmptyOrWhitespace(
  value: string | null | undefined,
): string | null {
  return typeof value === 'string'
    ? (value.trim() === '' ? null : value)
    : null
}
