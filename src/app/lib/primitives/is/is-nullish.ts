export function isNullish(a: any): a is null {
  return a === null || a === undefined
}
