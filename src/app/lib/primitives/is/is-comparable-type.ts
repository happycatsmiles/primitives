const comparableTypes = new Set([
  'bigint',
  'boolean',
  'function',
  'number',
  'string',
  'symbol',
  'undefined',
])

/**
 * Can this value be directly compared with `===`?
 */
export function isComparableType(value: any): boolean {
  return value === null || comparableTypes.has(typeof value)
}
