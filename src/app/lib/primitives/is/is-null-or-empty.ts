/**
 * Adds C#-ish "IsNullOrEmpty" that handles JavaScript's quirkiness.
 *
 * @param value
 */
export function isNullOrEmpty(value: string | null): value is null {
  return typeof value !== 'string' || value === ''
}

