import { isNullish } from './is-nullish'
import { Pojo } from '../types/pojo'

/**
 * Is this a POJO?
 * This excludes special objects such as `null`.
 *
 * @param value
 */
export function isPojo(value: any): value is Pojo {
  return typeof value === 'object' && !isNullish(value)
}
