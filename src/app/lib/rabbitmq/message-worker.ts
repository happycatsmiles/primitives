import { Channel, ConsumeMessage } from 'amqplib'

import { RabbitmqWorker } from './rabbitmq-worker'

const debug = require('debug')('lib:rabbitmq:messageWorker_')

/**
 * This is a helper object for `MessageService` listeners.
 *
 * This accepts a message from a queue on a channel,
 * decodes the payload and calls the worker with the payload.
 *
 * @param channel - channel the message was received on
 * @param rabbitMessage - the received message
 * @param worker - function to handle the message
 * @param workerHint - name for the worker to use in debug messages.
 */
export async function messageWorker_<PayloadType>(
  channel: Channel,
  rabbitMessage: ConsumeMessage | null,
  worker: RabbitmqWorker<PayloadType>,
  workerHint?: string,
): Promise<void> {
  if (!rabbitMessage) return

  const routingKey = rabbitMessage.fields?.routingKey

  try {
    const payload: PayloadType = rabbitMessage.content.length > 0
      ? JSON.parse(rabbitMessage.content.toString())
      : {}
    await worker(payload, routingKey)
  } catch (e) {
    debug(`Caught unexpected exception from worker ${workerHint ?? '[unnamed worker]'}:`, e)
  } finally {
    channel.ack(rabbitMessage)
  }
}
