export interface QueueSendObjectOptions {
  /**
   * This is the timeout, in milliseconds.
   *
   * **NOTE:** This value must be converted to a `string`.
   */
  expiration?: string
  headers?: any
}

