import { Connection, ConsumeMessage } from 'amqplib'

import { messageWorker_ } from './message-worker'
import { RabbitmqWorker } from './rabbitmq-worker'

const amqp = require('amqplib')

const debug = require('debug')('lib:rabbitmq:MessageService')

/**
 * Wrap some basic RabbitMQ boilerplate.
 */
export class MessageService {

  constructor(
    private connectionString: string,
  ) {
  }

  /**
   * Create a connection to the RabbitMQ server.
   */
  public connect_ = async (): Promise<Connection> => {
    return amqp.connect(this.connectionString)
  }

  /**
   * Set up a RabbitMQ exchange listener using exclusive queues.
   *
   * The expected message payload is a single serialized POJO.
   *
   * The actual work is done by an asynchronous function that
   * receives the deserialized payload.
   *
   * Note this leaves the connection open indefinitely.
   *
   * @param exchangeName - exchange to connect to
   * @param routingPatterns - routing patterns to listen on
   * @param worker - the function to call with the received POJO
   * @param exchangeType - the exchange type to use
   */
  public async setupExchangeListener_<T>(
    exchangeName: string,
    routingPatterns: string[],
    worker: RabbitmqWorker<T>,
    exchangeType = 'topic',
  ): Promise<void> {
    if (routingPatterns.length === 0)
      return

    const connection = await this.connect_()
    const channel = await connection.createChannel()
    await channel.assertExchange(
      exchangeName,
      exchangeType,
      {durable: true},
    )

    // Use an exclusive (temporary) queue to connect this worker.
    // The empty string tells RabbitMQ to generate a random name.
    const queue = await channel.assertQueue('', {exclusive: true})

    // Perform routing pattern bindings.
    for await (const i of routingPatterns) {
      await channel.bindQueue(
        queue.queue,
        exchangeName,
        i,
      )
    }

    await channel.prefetch(1)

    await channel.consume(
      queue.queue,
      (message: ConsumeMessage | null) =>
        messageWorker_<T>(channel, message, worker)
          .catch(err => this.exchangeExceptionHandler(err, exchangeName, routingPatterns)),
      {noAck: false},
    )
  }

  /**
   * Set up a RabbitMQ listener using named queues.
   *
   * Multiple instances listening on the same named queue will
   * cooperatively consume messages one by one.
   *
   * The expected message payload is a single serialized POJO.
   *
   * The actual work is done by an asynchronous function that
   * receives the deserialized payload.
   *
   * Note this leaves the connection open indefinitely.
   *
   * @param queueName - queue name to connect to
   * @param worker - the function to call with the received POJO
   */
  public async setupQueueListener_<T>(
    queueName: string,
    worker: RabbitmqWorker<T>,
  ): Promise<any> {
    const connection = await this.connect_()
    const channel = await connection.createChannel()

    const queue = await channel.assertQueue(queueName, {durable: true})
    await channel.prefetch(1)

    await channel.consume(
      queue.queue,
      (message: ConsumeMessage | null) =>
        messageWorker_<T>(channel, message, worker, queueName)
          .catch(err => this.queueExceptionHandler(err, queueName)),
      {noAck: false},
    )
  }

  private exchangeExceptionHandler(
    err: any,
    exchangeName: string,
    routingPatterns: string[],
  ) {
    debug(
      'exchangeExceptionHandler',
      'Uncaught exception in worker:',
      'exchangeName', exchangeName,
      'routingPatterns', routingPatterns,
      'err', err,
    )
  }

  private queueExceptionHandler(
    err: any,
    queueName: string,
  ) {
    debug(
      'queueExceptionHandler',
      'Uncaught exception in worker:',
      'queueName', queueName,
      'err', err,
    )
  }
}
