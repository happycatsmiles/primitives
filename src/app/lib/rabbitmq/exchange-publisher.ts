import { Channel, Connection } from 'amqplib'
import { setTimeout } from 'timers/promises'

import { ExchangePublisherOptions } from './exchange-publisher-options'

/**
 * This helper class simplifies sending a POJO to a RabbitMQ exchange.
 *
 * Use:
 *
 * ```
 const sender = new ExchangePublisher({
    exchangeName: ExchangeNames.TerritoryCapture,
    routeKey: RouteKeys.TerritoryCapture,
  })
 await sender.open_()
 try {
   await sender.sendObject_(resources)
 } finally {
   await sender.close_()
 }
 * ```
 */
export class ExchangePublisher {
  private connection: Connection
  private channel: Channel
  private isOpen = false

  constructor(private options: ExchangePublisherOptions) {
  }

  /**
   * Tear down the connection to RabbitMQ.
   */
  public async close_(): Promise<void> {
    await this.connection.close()
    this.isOpen = false
  }

  /**
   * Set up the connection to RabbitMQ.
   *
   * This must be paired with a call to `close_()`.
   */
  public async open_(): Promise<void> {
    this.connection = await this.options.messageService.connect_()
    this.channel = await this.connection.createChannel()

    await this.channel.assertExchange(
      this.options.exchangeName,
      this.options.exchangeType ?? 'topic',
      {durable: true},
    )

    this.isOpen = true
  }

  /**
   * Send a POJO to the RabbitMQ server.
   */
  public async sendObject_(object?: object | undefined): Promise<void> {
    if (!this.isOpen) return

    const json = JSON.stringify(object ?? {})

    this.channel.publish(
      this.options.exchangeName,
      this.options.routeKey,
      Buffer.from(json),
      {persistent: true},
    )

    // It appears that in order to actually send,
    // the JavaScript loop needs to complete its cycle.
    // It appears nothing will be sent if this isn't included
    await setTimeout(0)
  }
}

