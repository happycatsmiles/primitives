import { Channel, Connection } from 'amqplib'
import { setTimeout } from 'timers/promises'

import { MessageService } from './message.service'
import { QueueSendObjectOptions } from './queue-send-object-options'

/**
 * This helper class simplifies sending a POJO to a RabbitMQ queue.
 *
 * Use:
 *
 * ```
 const sender = new QueueSender({
    queueName: QueueNames.TerritoryCapture,
  })
 await sender.open_()
 try {
   await sender.sendObject_(resources)
 } finally {
   await sender.close_()
 }
 * ```
 */
export class QueueSender {
  private connection: Connection
  private channel: Channel
  private isOpen = false

  constructor(
    private messageService: MessageService,
    private queueName: string,
  ) {
  }

  /**
   * Tear down the connection to RabbitMQ.
   */
  public async close_(): Promise<void> {
    await this.connection.close()
    this.isOpen = false
  }

  /**
   * Set up the connection to RabbitMQ.
   */
  public async open_(): Promise<void> {
    this.connection = await this.messageService.connect_()
    this.channel = await this.connection.createChannel()

    await this.channel.assertQueue(
      this.queueName,
      {durable: true},
    )

    this.isOpen = true
  }

  /**
   * Send a POJO to the RabbitMQ server.
   */
  public async sendObject_(
    object?: object | undefined,
    options?: QueueSendObjectOptions,
  ): Promise<void> {
    if (!this.isOpen) return

    const json = JSON.stringify(object ?? {})
    const headers = options?.headers
    const expiration = options?.expiration

    this.channel.sendToQueue(
      this.queueName,
      Buffer.from(json),
      {
        persistent: true,
        expiration,
        headers,
      },
    )

    // It appears that in order to actually send,
    // the JavaScript loop needs to complete its cycle.
    // It appears nothing will be sent if this isn't included
    await setTimeout(0)
  }
}

