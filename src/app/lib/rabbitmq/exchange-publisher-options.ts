import { ExchangeTypes } from './exchange-types'
import { MessageService } from './message.service'

export interface ExchangePublisherOptions {
  messageService: MessageService,
  exchangeName: string,
  exchangeType?: ExchangeTypes,
  routeKey: string,
}

