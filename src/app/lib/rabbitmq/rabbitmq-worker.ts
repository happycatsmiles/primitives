/**
 * This is the type for a RabbitMQ worker.
 *
 * The worker receives an optional payload from the queue,
 * along with an optional routing key.
 */
export type RabbitmqWorker<PayloadType> = (
  object?: PayloadType,
  routingKey?: string,
) => Promise<void>
