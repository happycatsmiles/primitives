# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.5.0](https://gitlab.com/happycatsmiles/primitives/compare/v1.4.1...v1.5.0) (2023-01-14)


### Features

* add RabbitMQ common code. ([f3635be](https://gitlab.com/happycatsmiles/primitives/commit/f3635be5d6191305d05f5e0d53d3b380209a8f7f))

### [1.4.1](https://gitlab.com/happycatsmiles/primitives/compare/v1.4.0...v1.4.1) (2022-12-06)


### Bug Fixes

* rework spec files ([6d6974e](https://gitlab.com/happycatsmiles/primitives/commit/6d6974edabcd9f49baaf1b33aa6d1d62e05caa2b))

## [1.4.0](https://gitlab.com/happycatsmiles/primitives/compare/v1.3.1...v1.4.0) (2022-12-06)


### Features

* add eslint ([399518a](https://gitlab.com/happycatsmiles/primitives/commit/399518aaf6522f04fcb00a7cf52fd1ac28e6c9ef))


### Bug Fixes

* add test for BigInt larger than number. ([fe2aba2](https://gitlab.com/happycatsmiles/primitives/commit/fe2aba2afb9bbe1d3104feecc5eac14bce3776b3))
* fix forEach ([9665445](https://gitlab.com/happycatsmiles/primitives/commit/966544516fc0d6686f5d99651982a7675ad39897))
* remove unused variable ([c03ce55](https://gitlab.com/happycatsmiles/primitives/commit/c03ce55a44a06022f7c95cc577e65934bee9f42d))

### [1.3.1](https://gitlab.com/happycatsmiles/primitives/compare/v1.3.0...v1.3.1) (2022-11-30)


### Bug Fixes

* don't leave function case to chance. ([2cd9fbd](https://gitlab.com/happycatsmiles/primitives/commit/2cd9fbdb1a606cb2efd467f0d38f1c06128fe978))
