# Primitives

This is a collection of functions common to multiple projects, along with
testing.

## Location

The root for the collection is `./src/app/lib/primitives`.

## Organization

### Comparisons Subdirectory

The `comparisons` subdirectory contains functions for comparing data.

* `compareNumbers` - compare two numbers, returning `-1 | 0 | 1`.
* `compareStrings` - compare two strings, returning `-1 | 0 | 1`.
* `compareValues` - compare two values which can be compared with the `>`
  operator, returning `-1 | 0 | 1`.

### Conversions Subdirectory

The `conversions` subdirectory contains functions for converting from one data
type to another. These should be fairly rigorous.

* `toNumber` - convert to a `number`, or `null` if the conversion fails.
* `toNumberWithDefault` - convert to a `number`, or a default if the conversion
  fails.
* `toString` - convert to a `string`.

### DateTime Subdirectory

The `datetime` subdirectory contains functions for dealing with dates/times.

* `daysToMilliseconds` - convert a number of days to milliseconds. Negative
  values preserve the negative sign.
* `millisecondstoDays` - convert a number of milliseconds to days. Negative
  values preserve the negative sign.

### Is Subdirectory

The `is` subdirectory contains functions for determining whether a value
matches a certain criteria. For example, whether a value is a number, a
null-like value, etc.

These all return `true` or `false`.

* `isComparableType` - is a value's type able to be compared with the `===`
  operator
* `isNullOrEmpty` - is a value `null` or an empty (zero-length) string
* `isNullish` - is a value "null-like", e.g. `null` or `undefined`
* `isNumber` - is a value a `number`
* `isPojo` - is a value a POJO (plain old JavaScript object)
* `isString` - is a value a string
* `isValidNumber` - is a value a real number

### Sets Subdirectory

The `sets` subdirectory contains functions for set operations.

* `SetOperator` - a class providing set operators

### Types Subdirectory

The `types` subdirectory contains extra useful type declarations relevant to
the rest of the library.

